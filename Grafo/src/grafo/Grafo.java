package grafo;

import java.util.ArrayList;

/**
 * Grafo implementado como matriz de adyacencia
 * @author Martin
 *
 */
public class Grafo {
	
	protected boolean[][] _adyacencia; //matriz de adyacencia
	protected int _vertices; //cantidad de vertices
	
	/**
	 * Devuelve un grafo sin aristas con la cantidad de vertices indicada
	 * @param vertices
	 */
	public Grafo(int vertices){
		this._adyacencia = new boolean[vertices][vertices];
		this._vertices = vertices;
	}
	
	/**
	 * Agrega la arista ij que indice sobre los vertices i y j.
	 * No puede contener bucles
	 * Un vertice debe ser mayor a 0 y menor a la cantidad de vertices
	 * @param i vertice
	 * @param j vertice
	 */
	public void agregarArista(int i, int j){
		
		_verificarArista(i, j, "agregar");
		
		this._adyacencia[i][j] = true;
		this._adyacencia[j][i] = true;
	}
	
	/**
	 * Elimina la arista ij que indice sobre los vertices i y j.
	 * Un vertice debe ser mayor a 0 y menor a la cantidad de vertices
	 * @param i vertice
	 * @param j vertice
	 */
	public void eliminarArista(int i, int j){
		
		_verificarArista(i, j, "eliminar");
		
		this._adyacencia[i][j] = false;
		this._adyacencia[j][i] = false;
	}
	
	/**
	 * Controlar si la arista existe
	 * @param i
	 * @param j
	 * @return true si esta, false si no esta
	 */
	public boolean existeArista(int i, int j){
		
		_verificarArista(i, j, "consular");
		
		return _adyacencia[i][j];
	}
	
	/**
	 * Controla que la arista sea valida. IllegalArgumentException
	 * @param i
	 * @param j
	 * @param mensaje
	 */
	protected void _verificarArista(int i, int j, String tipo) {
		
		String mensaje = "Se intento "+ tipo+ "una arista invalida.";
		
		if (i == j)
			throw new IllegalArgumentException(mensaje);
		
		if (i < 0 || i >= this.cantidadVertices())
			throw new IllegalArgumentException(mensaje);
		
		if (j < 0 || j >= this.cantidadVertices())
			throw new IllegalArgumentException(mensaje);
	}
	
	/**
	 * Devuelve un array list con los vecino
	 * @return ArrayList<Integer> con los vecinos
	 */
	public ArrayList<Integer> vecinos(int vertice){
		
		_verificarVertice(vertice, "consultar");
		
		ArrayList<Integer> ret = new ArrayList<Integer>();
		for(int col = 0; col < this.cantidadVertices(); col++){
			if(vertice != col && this.existeArista(vertice,col)){
				ret.add(col);
			}
		}
		return ret;
	}
	
	/**
	 * Devuelve el grado del vertice i
	 * @param i vertice 
	 * @return int cantidad de vertices
	 */
	public int grado(int i){
		return this.vecinos(i).size();
	}
	
	/**
	 * Verifica que un vertice sea valido. IllegalArgumentException
	 * @param i
	 * @param tipo
	 */
	protected void _verificarVertice(int i, String tipo){
		
		String mensaje = "Se intento " + tipo + "un vertice invalido"; 
		
		if (i < 0 || i >= this.cantidadVertices())
			throw new IllegalArgumentException(mensaje);
	}
	
	/**
	 * Devuelve la cantidad de vertices de una grafo
	 * @return int cantidad de vertices
	 */
	public int cantidadVertices() {
		return _vertices;
	}
	
	//escribo esto aca para verificar

}
