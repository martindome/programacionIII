package prueba;

import grafo.Grafo;
import java.util.ArrayList;

import static org.junit.Assert.*;
import org.junit.*;

import org.junit.Test;

public class GrafoTest {
	
	@Before
	public void setup(){
		//Aca pondrias un setup
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void agregarAristaFueraDeRangoSuperiorI(){
		
		//setup
		Grafo g = new Grafo(5);
		
		//Exercice
		g.agregarArista(5,0);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void agregarAristaFueraDeRangoSuperiorJ(){
		
		//setup
		Grafo g = new Grafo(5);
		
		//Exercice
		g.agregarArista(0,5);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void agregarAristaFueraDeRangoInferior(){
		
		//setup
		Grafo g = new Grafo(5);
		
		//Exercice
		g.agregarArista(-1,0);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void agregarAristaFueraDeRangoInferiorJ(){
		
		//setup
		Grafo g = new Grafo(5);
		
		//Exercice
		g.agregarArista(0,-1);
	}

	@Test
	public void agregarAristaTest() {
		
		//Setup
		Grafo g = new Grafo(5);
		
		//Execution, Exercice
		g.agregarArista(0,1);
		
		//Verification
		assertTrue(g.existeArista(0,1));
	}
	
	@Test
	public void agregarAristaSimetriaTest() {
		
		//Setup
		Grafo g = new Grafo(5);
		
		//Execution, Exercice
		g.agregarArista(0,1);
		
		//Verification
		assertTrue(g.existeArista(1,0));
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void agregarAristaVerticeIgual(){
		
		//setup
		Grafo g = new Grafo(5);
		
		//Exercice
		g.agregarArista(0,0);
	}
	
	@Test
	public void eliminarAristaTest(){
		
		Grafo g = new Grafo(5);
		g.agregarArista(1, 0);
		
		g.eliminarArista(1,0);
		assertFalse(g.existeArista(1, 0));
	}
	
	
	//No lo mando a un @Before porque no todos los test usan este grafo
	public Grafo inicializarEjemplo() {
		Grafo g = new Grafo(5);
		g.agregarArista(1,0);
		g.agregarArista(1,2);
		g.agregarArista(1,3);
		g.agregarArista(3,2);
		g.agregarArista(0,2);
		return g;
	}
	
	@Test
	public void gradoTest(){
		Grafo g = inicializarEjemplo();
		
		assertTrue(g.grado(1) == 3);
	}
	
	@Test
	public void gradoCeroTest(){
		Grafo g = inicializarEjemplo();
		
		assertTrue(g.grado(4) == 0);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void gradoTestFueraRango(){
		Grafo g = this.inicializarEjemplo();
		g.grado(5);
	}
	
	@Test
	public void vecinosTestCantidad(){
		
		//Setup
		Grafo g = new Grafo(5);
		g.agregarArista(0, 1);
		g.agregarArista(0, 2);
		g.agregarArista(0, 3);
		ArrayList<Integer> vecinos = g.vecinos(0);
		
		//Test
		assertEquals(3, vecinos.size());
	}
	
	@Test
	public void vecinosTest(){
		
		//Setup
		Grafo g = new Grafo(5);
		g.agregarArista(0, 1);
		g.agregarArista(0, 2);
		g.agregarArista(0, 3);
		ArrayList<Integer> vecinos = g.vecinos(0);
		
		//Test
		assertTrue(g.vecinos(0).contains(1));
		assertTrue(g.vecinos(0).contains(2));
		assertTrue(g.vecinos(0).contains(3));
		
		
		
	}
	
	
	

}
