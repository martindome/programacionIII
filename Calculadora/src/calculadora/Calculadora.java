package calculadora;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.SwingConstants;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;
import org.eclipse.wb.swing.FocusTraversalOnArray;
import java.awt.Component;

public class Calculadora {

	private JFrame frame;
	private JTextField fieldSegundoOp;
	private JTextField fieldPrimerOp;
	private JTextField fieldResultado;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Calculadora window = new Calculadora();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Calculadora() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(new Color(128, 128, 128));
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblPrimerOperando = new JLabel("Primer Operando:");
		lblPrimerOperando.setBounds(10, 11, 126, 14);
		frame.getContentPane().add(lblPrimerOperando);
		
		JLabel lblSegundoOperando = new JLabel("Segundo Operando:");
		lblSegundoOperando.setBounds(10, 62, 126, 14);
		frame.getContentPane().add(lblSegundoOperando);
		
		JLabel lblResultado = new JLabel("Resultado:");
		lblResultado.setBounds(10, 152, 126, 14);
		frame.getContentPane().add(lblResultado);
		
		fieldSegundoOp = new JTextField();
		fieldSegundoOp.setHorizontalAlignment(SwingConstants.RIGHT);
		fieldSegundoOp.setBounds(146, 59, 86, 20);
		frame.getContentPane().add(fieldSegundoOp);
		fieldSegundoOp.setColumns(10);
		
		fieldPrimerOp = new JTextField();
		fieldPrimerOp.setHorizontalAlignment(SwingConstants.RIGHT);
		fieldPrimerOp.setBounds(146, 8, 86, 20);
		frame.getContentPane().add(fieldPrimerOp);
		fieldPrimerOp.setColumns(10);
		
		fieldResultado = new JTextField();
		fieldResultado.setEditable(false);
		fieldResultado.setHorizontalAlignment(SwingConstants.RIGHT);
		fieldResultado.setBounds(146, 149, 86, 20);
		frame.getContentPane().add(fieldResultado);
		fieldResultado.setColumns(10);
		
		JButton buttonSuma = new JButton("+");
		buttonSuma.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String suma = Integer.toString(Integer.parseInt(fieldPrimerOp.getText()) + Integer.parseInt(fieldSegundoOp.getText()) );
				fieldResultado.setText(suma);
			}
		});
		buttonSuma.setBounds(283, 7, 89, 23);
		frame.getContentPane().add(buttonSuma);
		
		JButton buttonResta = new JButton("-");
		buttonResta.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String resta = Integer.toString(Integer.parseInt(fieldPrimerOp.getText()) - Integer.parseInt(fieldSegundoOp.getText()) );
				fieldResultado.setText(resta);
			}
		});
		buttonResta.setBounds(283, 41, 89, 23);
		frame.getContentPane().add(buttonResta);
		
		JButton buttonMultiplicacion = new JButton("*");
		buttonMultiplicacion.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String multiplicacion = Integer.toString(Integer.parseInt(fieldPrimerOp.getText()) * Integer.parseInt(fieldSegundoOp.getText()) );
				fieldResultado.setText(multiplicacion);
			}
		});
		buttonMultiplicacion.setBounds(283, 75, 89, 23);
		frame.getContentPane().add(buttonMultiplicacion);
		
		JButton buttonDivision = new JButton("/");
		buttonDivision.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String division = Double.toString(Double.parseDouble(fieldPrimerOp.getText()) / Double.parseDouble(fieldSegundoOp.getText()) );
				if (division.length() > 6) {
					String respuesta = division.substring(0, 5);
					fieldResultado.setText(respuesta);
				}
				else {
					fieldResultado.setText(division);
				}
			}
		});
		buttonDivision.setBounds(283, 113, 89, 23);
		frame.getContentPane().add(buttonDivision);
		frame.getContentPane().setFocusTraversalPolicy(new FocusTraversalOnArray(new Component[]{lblPrimerOperando, lblSegundoOperando, lblResultado, fieldSegundoOp, fieldPrimerOp, fieldResultado, buttonSuma, buttonResta, buttonMultiplicacion, buttonDivision}));
	}
}
